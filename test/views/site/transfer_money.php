<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use app\models\UserSearch;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Transfer Money';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="transfer-money col-md-4">

    <?php $form = ActiveForm::begin([
    ]) ?>
    <?= $form->field($model, 'balance')->textInput(['readonly' => true, 'value' => Yii::$app->user->identity->balance])->label('Your balance') ?>
    <?= $form->field($model, 'transfer_sum')->textInput()->label('Money to transfer') ?>
    <?= $form->field($model, 'id')->dropDownList(UserSearch::findUsers())->label('Choose user who you want to send money') ?>
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Send money', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>

</div>
