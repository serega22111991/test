<?php

use app\models\UserSearch;
use yii\grid\GridView;

    $this->title = 'Balance';
    $this->params['breadcrumbs'][] = $this->title;


?>


<div class="balance">

    <?= /** @var  $dataProvider */
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'user_get_id' => [
                    'value' => function($model){
                        return UserSearch::findUserById($model->user_get_id);
                    },
                    'label' => 'Recipient'
                ],
                'send_sum' => [
                    'value' => function($model){
                        return $model->send_sum;
                    },
                    'label' => 'Send_sum'
                ]
            ],
        ]);
    ?>

</div>
