<?php

    use yii\grid\GridView;

    $this->title = 'Balance';
    $this->params['breadcrumbs'][] = $this->title;

//    print_r($dataProvider->query->asarray()->all());
//    die;

?>


<div class="balance">

    <?= /** @var  $dataProvider */
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'username',
                'balance'
            ],
        ]);
    ?>

</div>
