<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "send_money_history".
 *
 * @property int $id
 * @property int $user_send_id
 * @property int $user_get_id
 * @property int $send_sum
 */
class SendMoneyHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'send_money_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_send_id', 'user_get_id', 'send_sum'], 'required'],
            [['user_send_id', 'user_get_id', 'send_sum'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_send_id' => 'User Send ID',
            'user_get_id' => 'User Get ID',
            'send_sum' => 'Send Sum',
        ];
    }
}
