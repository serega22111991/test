<?php

namespace app\models;

use Yii;

class TransferMoneyForm extends UserSearch
{
    public $transfer_sum;
    public $new_send_user_balance;
    public $new_get_user_balance;

    public function rules()
    {
        return [
            [['id', 'balance', 'transfer_sum'], 'required'],
            [['id'], 'integer'],
            [['balance', 'transfer_sum'], 'double'],
            [['transfer_sum'], 'validate_transfer_sum'],
            [['id'], 'validate_user_get_money'],
            [['balance'], 'validate_balance_user_send_money'],
        ];
    }

    public function validate_transfer_sum($attribute, $params){

        if($this->$attribute < 0){
            $this->addError($attribute, 'You can send only plus sum ');
        }
    }


    public function validate_user_get_money($attribute, $params){
        $user = self::findOne($this->id);
        if(!$user){
            $this->addError($attribute, 'User don\'t find ');
        }

        if($this->id == Yii::$app->user->identity->id){
            $this->addError($attribute, 'You can\'t send to yourself');
        }
    }

    public function validate_balance_user_send_money($attribute, $params)
    {
        $user_send_balance = Yii::$app->user->identity->balance ;
        $user_get_balance = $this->findUserBalance($this->id);

        if($user_send_balance - $this->transfer_sum >= -1000){

            $this->new_send_user_balance = $user_send_balance - $this->transfer_sum;
            $this->new_get_user_balance = $user_get_balance + $this->transfer_sum;

        }else{
            $this->addError($attribute, 'You don\'t have enough money');
        }
    }

    public function updateBalances()
    {

        User::updateAll(['balance' => $this->new_send_user_balance ], ['id' => Yii::$app->user->identity->id]);
        User::updateAll(['balance' => $this->new_get_user_balance], ['id' => $this->id]);
        $this->writeHistory();

        return true;
    }

    public function writeHistory()
    {
        Yii::$app->db->createCommand('INSERT INTO `send_money_history` (`user_send_id`, `user_get_id`, `send_sum` ) 
        VALUES (:user_send_id, :user_get_id, :send_sum)', [
            ':user_send_id' => Yii::$app->user->identity->id,
            ':user_get_id' => $this->id,
            ':send_sum' => $this->transfer_sum,
        ])->execute();
    }
}
