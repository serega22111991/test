<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $access_token
 * @property string $auth_key
 */
class UserSearch extends User
{

    public function searchUserBalance()
    {
        $query = $this->findUsersBalance();

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $provider;
    }
    public function findUserBalance($id)
    {
        $user = self::find()->select('balance')->where(['id' => $id])
            ->limit(1)
            ->asArray()->one();

        return $user['balance'];
    }

    public function findUsersBalance()
    {
        return self::find()->select(['username','balance']);
    }

    public static function findUsers()
    {
        $users = self::find()->select(['id', 'username'])->asArray()->all();
        return ArrayHelper::map($users, 'id', 'username');
    }

    public static function findUserById($id)
    {
        $user = self::find()->where(['id' => $id])
            ->limit(1)
            ->asArray()->one();

        return $user['username'];
    }
}