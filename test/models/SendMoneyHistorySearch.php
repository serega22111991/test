<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;


class SendMoneyHistorySearch extends SendMoneyHistory
{
    public function searchUserTransfer()
    {
        $query = $this->findUserTransfers();


        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $provider;
    }


    public function findUserTransfers()
    {

        return self::find()->where(['user_send_id' => Yii::$app->user->identity->id]);

    }
}
