<?php

use yii\db\Migration;

/**
 * Class m190721_172628_add_table_user
 */
class m190721_172628_add_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $this->execute("
        
            CREATE TABLE `user` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `username` VARCHAR(50) NULL DEFAULT NULL,
                `password` VARCHAR(50) NULL DEFAULT NULL,
                `access_token` VARCHAR(50) NULL DEFAULT NULL,
                `auth_key` VARCHAR(50) NULL DEFAULT NULL,
                `balance` DECIMAL(10,2) NULL DEFAULT '0.00',
                PRIMARY KEY (`id`),
                UNIQUE INDEX `login` (`username`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            AUTO_INCREMENT=11;
                    
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
       $this->dropTable('user');
    }


}
