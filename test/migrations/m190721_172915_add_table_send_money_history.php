<?php

use yii\db\Migration;

/**
 * Class m190721_172915_add_table_send_money_history
 */
class m190721_172915_add_table_send_money_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $this->execute("

            CREATE TABLE `send_money_history` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `user_send_id` INT(10) NOT NULL,
                `user_get_id` INT(10) NOT NULL,
                `send_sum` INT(10) NOT NULL,
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            AUTO_INCREMENT=62
            ;

        ");
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {

        $this->dropTable('send_money_history');

    }

}
